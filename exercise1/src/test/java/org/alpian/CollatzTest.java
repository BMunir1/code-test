package org.alpian;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class CollatzTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    void collatz_10() {
        Math.collatz(10);
        assertEquals("10 5 16 8 4 2 1 ", outContent.toString());
    }

    @Test
    void collatz_12() {
        Math.collatz(12);
        assertEquals("12 6 3 10 5 16 8 4 2 1 ", outContent.toString());
    }

    @Test
    void collatz_1() {
        Math.collatz(1);
        assertEquals("1 ", outContent.toString());
    }

    @Test
    void collatz_0() {
        assertThrows(StackOverflowError.class, () -> {
            Math.collatz(0);
        });
    }
}