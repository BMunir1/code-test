package org.alpian;

import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CollatzTailTest {


    @Test
    void collatz_tail_1() {
        final var queue = new ArrayDeque<Integer>();
        Math.collatzTail(1, queue);
        assertEquals(1, queue.size());
        assertEquals("[1]", queue.toString());
    }

    @Test
    void collatz_tail_10() {
        final var queue = new ArrayDeque<Integer>();
        Math.collatzTail(10, queue);
        assertEquals(7, queue.size());
        assertEquals("[1, 2, 4, 8, 16, 5, 10]", queue.toString());
    }

    @Test
    void collatz_tail_12() {
        final var queue = new ArrayDeque<Integer>();
        Math.collatzTail(12, queue);
        assertEquals(10, queue.size());
        assertEquals("[1, 2, 4, 8, 16, 5, 10, 3, 6, 12]", queue.toString());
    }

    @Test
    void collatz_tail_0() {
        final var queue = new ArrayDeque<Integer>();
        assertThrows(StackOverflowError.class, () -> {
            Math.collatzTail(0, queue);
        });
    }
}
