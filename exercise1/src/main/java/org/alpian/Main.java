package org.alpian;

import java.util.ArrayDeque;

import static org.alpian.Math.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("------linear----");
        collatz(Integer.parseInt(args[0]));
        System.out.println(System.lineSeparator() + "-----tail-----");
        collatzTail(Integer.parseInt(args[0]), new ArrayDeque<>());
    }
}