package org.alpian;

import java.util.Deque;

public class Math {

    // I did not validate the n value as I was just trying to translate the function and did not want to add anything
    // If I were to validate I would check if the n > 1 if not throw illegal argument exception
    public static void collatz(int n) {
        System.out.printf("%d ", n);
        if (n == 1)
            return;
        if ((n % 2 == 0)) {
            collatz(n / 2);
        } else {
            collatz((3 * n) + 1);
        }
    }


    public static void collatzTail(int n, Deque<Integer> stack) {
        if (n == 1) {
            stack.push(n);
            System.out.println(stack);
        } else if ((n % 2 == 0)) {
            stack.push(n);
            collatzTail(n / 2, stack);
        } else {
            stack.push(n);
            collatzTail((3 * n) + 1, stack);
        }
    }
}
