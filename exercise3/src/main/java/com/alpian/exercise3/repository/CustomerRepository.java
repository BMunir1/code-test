package com.alpian.exercise3.repository;

import com.alpian.exercise3.Entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Optional<Customer> findByCustomerId(Integer customerId);

    boolean existsByCustomerId(Integer customerId);
}
