package com.alpian.exercise3.service;

import com.alpian.exercise3.exception.InvalidCustomerId;
import com.alpian.exercise3.model.CustomerRequest;
import com.alpian.exercise3.model.CustomerResponse;
import com.alpian.exercise3.model.ExternalIdResponse;
import com.alpian.exercise3.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

import static com.alpian.exercise3.mapper.CustomerMapper.toCustomerResponse;
import static com.alpian.exercise3.mapper.CustomerMapper.toCustomer;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerResponse saveCustomer(CustomerRequest customerRequest) {
        final var isCustomerAvailable = customerRepository.existsByCustomerId(customerRequest.getCustomerId());
        if (isCustomerAvailable) {
            throw new InvalidCustomerId(String.format("customer id not unique, this id already exists %d", customerRequest.getCustomerId()));
        }
        final var customer = customerRepository.save(toCustomer(customerRequest));
        return toCustomerResponse(customer);
    }

    public ExternalIdResponse getExternalIdForCustomer(Integer customerId) {
        final var optionalCustomer = customerRepository.findByCustomerId(customerId);
        return optionalCustomer
                .map(c -> new ExternalIdResponse(c.getExternalId()))
                .orElseThrow(() -> new EntityNotFoundException(String.format("no entity found for customer id: %d", customerId)));
    }
}
