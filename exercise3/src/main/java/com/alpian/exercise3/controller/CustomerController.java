package com.alpian.exercise3.controller;

import com.alpian.exercise3.model.CustomerRequest;
import com.alpian.exercise3.model.CustomerResponse;
import com.alpian.exercise3.model.ExternalIdResponse;
import com.alpian.exercise3.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;
import java.time.LocalDate;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
@Validated
public class CustomerController {

    private final CustomerService customerService;

    @GetMapping("/customers/{customerId}")
    public ResponseEntity<ExternalIdResponse> getExternalId(@PathVariable @Positive(message = "customer id must be positive") Integer customerId) {

        final var externalIdResponse = customerService.getExternalIdForCustomer(customerId);
        return ResponseEntity.ok(externalIdResponse);
    }

    @PostMapping("/customers")
    public ResponseEntity<CustomerResponse> createCustomer(@Param("createdAt") @PastOrPresent(message = "date must be in the past or today") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate createdAt, @Param("customerId") @Positive(message = "customer id must be positive") Integer customerId) {
        final var customerResponse = customerService.saveCustomer(new CustomerRequest(createdAt, customerId));
        return ResponseEntity.status(HttpStatus.CREATED).body(customerResponse);
    }
}
