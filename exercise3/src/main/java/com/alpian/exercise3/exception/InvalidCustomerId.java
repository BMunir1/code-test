package com.alpian.exercise3.exception;

public class InvalidCustomerId extends RuntimeException {
    public InvalidCustomerId(String message) {
        super(message);
    }
}
