package com.alpian.exercise3.model;

import lombok.Getter;

import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;

@Getter
public class CustomerRequest {
    private final LocalDate createdAt;
    private final int customerId;
    private final String externalId;

    /**
     * validate the user input for null and throws illegalArgumentException if empty
     */
    public CustomerRequest(LocalDate createdAt, Integer customerId) {
        if (Objects.isNull(customerId))
            throw new IllegalArgumentException("customer id can't be null");
        if (Objects.isNull(createdAt))
            throw new IllegalArgumentException("created at can't be null");
        this.createdAt = createdAt;
        this.customerId = customerId;
        this.externalId = UUID.randomUUID().toString();
    }

}
