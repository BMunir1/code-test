package com.alpian.exercise3.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Getter;

@Getter
public class ExternalIdResponse {
    private final String externalId;

    @JsonCreator
    public ExternalIdResponse(String externalId) {
        this.externalId = externalId;
    }
}
