package com.alpian.exercise3.model;

import lombok.Builder;
import lombok.Value;

import java.time.LocalDate;

@Value
@Builder
public class CustomerResponse {
    Integer customerId;
    String externalId;
    LocalDate createdAt;
}
