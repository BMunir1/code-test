package com.alpian.exercise3.model;

import lombok.Builder;
import lombok.Value;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Value
@Builder
public class ErrorResponse {
    String message;
    HttpStatus status;
    LocalDateTime time;
}
