package com.alpian.exercise3.mapper;

import com.alpian.exercise3.Entity.Customer;
import com.alpian.exercise3.model.CustomerRequest;
import com.alpian.exercise3.model.CustomerResponse;

public class CustomerMapper {

    private CustomerMapper() {}

    public static Customer toCustomer(CustomerRequest customerRequest) {
        return Customer.builder()
                .customerId(customerRequest.getCustomerId())
                .externalId(customerRequest.getExternalId())
                .createdAt(customerRequest.getCreatedAt())
                .build();
    }

    public static CustomerResponse toCustomerResponse(Customer customer) {
        return CustomerResponse.builder()
                .customerId(customer.getCustomerId())
                .createdAt(customer.getCreatedAt())
                .externalId(customer.getExternalId())
                .build();
    }
}
