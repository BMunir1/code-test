package com.alpian.exercise3.controller;

import com.alpian.exercise3.model.ErrorResponse;
import com.alpian.exercise3.service.CustomerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = CustomerController.class)
class CustomerControllerTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CustomerService customerService;

    @Test
    void invalidCustomerId() throws Exception {
        final var mvcResult = mockMvc.perform(get("/api/v1/customers/-1"))
                .andExpect(status().isBadRequest())
                .andReturn();

        final var errorResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), ErrorResponse.class);
        assertThat(errorResponse.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(errorResponse.getMessage()).isEqualTo("getExternalId.customerId: customer id must be positive");
    }

    @Test
    void createdAtDateInFuture() throws Exception {
        final var mvcResult = mockMvc.perform(post("/api/v1/customers")
                        .param("createdAt", LocalDate.now().plusDays(1).toString())
                        .param("customerId", "1"))
                .andExpect(status().isBadRequest())
                .andReturn();

        final var errorResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), ErrorResponse.class);
        assertThat(errorResponse.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(errorResponse.getMessage()).isEqualTo("createCustomer.createdAt: date must be in the past or today");
    }

    @Test
    void invalidCustomerIdForNewCustomer() throws Exception {
        final var mvcResult = mockMvc.perform(post("/api/v1/customers")
                        .param("createdAt", LocalDate.now().toString())
                        .param("customerId", "0"))
                .andExpect(status().isBadRequest())
                .andReturn();

        final var errorResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), ErrorResponse.class);
        assertThat(errorResponse.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(errorResponse.getMessage()).isEqualTo("createCustomer.customerId: customer id must be positive");
    }

    @Test
    void customerIdIsNull() throws Exception {
        final var mvcResult = mockMvc.perform(post("/api/v1/customers")
                        .param("createdAt", LocalDate.now().toString())
                        .param("customerId", ""))
                .andExpect(status().isBadRequest())
                .andReturn();

        final var errorResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), ErrorResponse.class);
        assertThat(errorResponse.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(errorResponse.getMessage()).isEqualTo("customer id can't be null");
    }

    @ParameterizedTest
    @ValueSource(strings = {"20-12-2020", "12-30-2020", "3-10-2020"})
    void invalidCreatedAtDateFormat(String createdAt) throws Exception {
        final var mvcResult = mockMvc.perform(post("/api/v1/customers")
                        .param("createdAt", createdAt)
                        .param("customerId", "1"))
                .andExpect(status().isBadRequest())
                .andReturn();

        final var errorResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), ErrorResponse.class);
        assertThat(errorResponse.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(errorResponse.getMessage())
                .contains("Failed to convert value of type 'java.lang.String' to required type 'java.time.LocalDate'",
                "Parse attempt failed for value");
    }

    @ParameterizedTest
    @NullAndEmptySource
    void createdAtDateIsNull(String createdAt) throws Exception {
        final var mvcResult = mockMvc.perform(post("/api/v1/customers")
                        .param("createdAt", createdAt)
                        .param("customerId", "1"))
                .andExpect(status().isBadRequest())
                .andReturn();

        final var errorResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), ErrorResponse.class);
        assertThat(errorResponse.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(errorResponse.getMessage()).contains("created at can't be null");
    }
}