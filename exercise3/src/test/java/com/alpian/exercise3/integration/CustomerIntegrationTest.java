package com.alpian.exercise3.integration;

import com.alpian.exercise3.Entity.Customer;
import com.alpian.exercise3.model.ErrorResponse;
import com.alpian.exercise3.model.ExternalIdResponse;
import com.alpian.exercise3.repository.CustomerRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class CustomerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CustomerRepository repository;

    @Autowired
    private ObjectMapper mapper;

    @Test
    void createCustomer() throws Exception {
        mockMvc.perform(post("/api/v1/customers")
                        .param("createdAt", LocalDate.now().toString())
                        .param("customerId", "1"))
                .andExpect(status().isCreated())
                .andReturn();
        final var isCustomerAvailable = repository.existsByCustomerId(1);
        assertThat(isCustomerAvailable).isTrue();
        final var customer = repository.findByCustomerId(1).orElseThrow();
        assertThat(customer.getCreatedAt()).isEqualTo(LocalDate.now().toString());
        assertThat(customer.getCustomerId()).isOne();
        assertThat(customer.getExternalId()).isNotBlank();
    }

    @Test
    void getExternalIdForCustomer() throws Exception {

        repository.save(getCustomer().build());

        final var mvcResult = mockMvc.perform(get("/api/v1/customers/1"))
                .andExpect(status().isOk())
                .andReturn();

        final var externalIdResponse = mapper.readValue(mvcResult.getResponse().getContentAsString(), ExternalIdResponse.class);
        assertThat(externalIdResponse.getExternalId()).isEqualTo("random");
    }

    private static Customer.CustomerBuilder getCustomer() {
        return Customer.builder()
                .customerId(1)
                .externalId("random")
                .createdAt(LocalDate.now());
    }

    @Test
    void customerNotFound() throws Exception {
        final var mvcResult = mockMvc.perform(get("/api/v1/customers/1"))
                .andExpect(status().isNotFound())
                .andReturn();

        final var errorResponse = mapper.readValue(mvcResult.getResponse().getContentAsString(), ErrorResponse.class);
        assertThat(errorResponse.getMessage()).isEqualTo("no entity found for customer id: 1");
        assertThat(errorResponse.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void customerIdAlreadyExists() throws Exception {
        repository.save(getCustomer().build());

        final var mvcResult = mockMvc.perform(post("/api/v1/customers")
                        .param("createdAt", LocalDate.now().toString())
                        .param("customerId", "1"))
                .andExpect(status().isBadRequest())
                .andReturn();

        final var errorResponse = mapper.readValue(mvcResult.getResponse().getContentAsString(), ErrorResponse.class);
        assertThat(errorResponse.getMessage()).isEqualTo("customer id not unique, this id already exists 1");
        assertThat(errorResponse.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
    }
}
