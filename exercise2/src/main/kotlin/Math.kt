/**
 * coming from java I wanted to write a static method
 * when reading the docs I learnt this is the recommended way to do it
 */
fun dotProduct(vector1: List<Double>, vector2: List<Double>): Double {
    if (vector1.size != vector2.size) {
        throw IllegalArgumentException("vectors size are not equal, vector1: ${vector1.size} vector2: ${vector2.size}")
    }
    if (vector1.isEmpty()) {
        throw IllegalArgumentException("vectors are empty, dot product cant be applied")
    }
    return vector1
        .zip(vector2)
        .map { it.first * it.second }
        .reduce { sum, value -> sum + value }
}