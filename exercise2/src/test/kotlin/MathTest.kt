import org.junit.jupiter.api.Assertions.assertEquals
import kotlin.test.Test
import kotlin.test.assertFailsWith

internal class MathTest {

    @Test
    fun dotProductWithThreeValues() {
        val expected = 3.0
        assertEquals(expected, dotProduct(listOf(1.0,1.0,1.0), listOf(1.0,1.0,1.0)))
    }

    @Test
    fun dotProductWithTwoValues() {
        val expected = 8.0
        assertEquals(expected, dotProduct(listOf(2.0,2.0), listOf(2.0,2.0)))
    }

    @Test
    fun dotProductWithNegativeValues() {
        val expected = -1.0
        assertEquals(expected, dotProduct(listOf(1.5,-4.0,-2.0), listOf(2.0,2.0,-2.0)))
    }

    @Test
    fun dotProductWithZeroValues() {
        val expected = 4.0
        assertEquals(expected, dotProduct(listOf(0.0,2.0), listOf(0.0,2.0)))
    }

    @Test
    fun emptyValuesThrowsException() {
        val exception = assertFailsWith<IllegalArgumentException>(
            block = {
                dotProduct(listOf(), listOf())
            })
        assertEquals("vectors are empty, dot product cant be applied", exception.message)
    }

    @Test
    fun differentLengthVectorsThrowsException() {
        val vector1 = listOf(1.0, 2.0, 3.0)
        val vector2 = listOf(1.0, 2.0)
        val exception = assertFailsWith<IllegalArgumentException>(
            block = {
                dotProduct(vector1, vector2)
            })
        assertEquals("vectors size are not equal, vector1: 3 vector2: 2", exception.message)
    }
}

